import React from "react";

const Button = ({label="Submit", className="btn btn-primary", onClick=null}) => {
    return (
        <button type="button" className={className} onClick={onClick}>{label}</button>
    )
}

export default Button