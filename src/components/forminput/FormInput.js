import React from "react";


const FormInput = ({term, setTerm}) => {

    const onFormSubmit= (event) => {
        event.preventDefault();
    }

    return (
        <input type="text" value={term} onChange={(e) => setTerm(e.target.value)} />
    )
}

export default FormInput