import React, {useState} from "react";
import Button from "../button/Button";
import SearchBar from "../searchbar/SearchBar";
import "./header.css"

const Header = () => {

    const [autoCompleteresults, setAutoCompleteresults] = useState([])

    const apiResults = [
        {
            "id": "1",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "2",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "3",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "4",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "6",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "7",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        },
        {
            "id": "8",
            "content" : "fsdjfglsdfulszefuzdlfpsedfhszfhsdofsdfgmdsvms"
        }
    ]

    const FetchAutoCompleteResults = (filterObject) => {
        // make api call to fetch auto complete results
        let randInt = Math.random() * 1000
        setAutoCompleteresults(apiResults.slice(0, (randInt%6)+1))
    }

    const getNavigationPath = () => {
        // derive nav path
        return "Client Congigurator > Consumer & Brand > Brand Name > UI Text Editor"
    }

    return (
        <div>
            <div className="d-flex flex-row mb-3">
                <div className="p-2" style={{fontSize:"12px"}}>{getNavigationPath()}</div>
            </div>
            <div className="d-flex flex-row mb-3">
                <div className="p-2">UI Text Editor</div>
                <div className="p"><Button label="QA" className="btn btn-primary"/></div>
                <div className="p"><Button label="Semi" className="btn btn-secondary"/></div>
                <div className="p-1"><SearchBar autoCompleteresults={autoCompleteresults} FetchAutoCompleteResults={FetchAutoCompleteResults}/></div>
            </div>
        </div>
    )
}

export default Header