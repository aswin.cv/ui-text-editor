import React, {useEffect, useState} from "react";
import Button from "../button/Button";
import DropDown from "../dropdown/DropDown"
import FormInput from "../forminput/FormInput";

const SearchBar = ({defaultText, autoCompleteresults=[], FetchAutoCompleteResults=null}) => {
    const [section, setSection] = useState(0)
    const [filterBy, setFilterBy] = useState(0)
    const [term, setTerm] = useState()

    console.log(autoCompleteresults)
    const availableSections = [
        {
            label: "Section 1",
            value: "section1"
        },
        {
            label: "Section 2",
            value: "section2"
        }
    ]

    const availableOptionsForFilter = [
        {
            label: "Id",
            value: "id"
        },
        {
            label: "Content",
            value: "content"
        }
    ]

    const onSubmit = (event) => {
        event.preventDefault();
    }

    useEffect( () => {
        const search = ()=>{
            if(FetchAutoCompleteResults && term){
                FetchAutoCompleteResults({term: term, section:availableSections[section], filterBy:availableOptionsForFilter[filterBy]})
            }
        }
        const timer = setTimeout( () => {
            if(term){search()}
        }, 500);
        return () => {
            clearTimeout(timer)
        }
    }, [term, section, filterBy])

    return (
        <div>
            <form role="search" style={{display: "block"}}>
                <FormInput term={term} setTerm={setTerm}/>
                <DropDown options={availableSections} label={"Section"} selectedOption={section} setSelectedOption={setSection} />
                <DropDown options={availableOptionsForFilter} label={"FilterBy"} selectedOption={filterBy} setSelectedOption={setFilterBy} />
                <Button label="Search" className="btn btn-primary" onCdivck={onSubmit}/>
            </form>
            <div className="dropdown">
                {
                    autoCompleteresults.map((option, index) => {
                        return (
                            <div key={option.id} className="dropdown-item">
                                <div onCdivck={()=>{}}>{`ID:${option.id} Content:${option.content}`}</div>
                            </div>
                        )
                    })
                }   
            </div>
        </div>
    )
}

export default SearchBar