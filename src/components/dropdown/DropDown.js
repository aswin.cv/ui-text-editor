import React from "react";


const DropDown = ({options, label, selectedOption, setSelectedOption}) => {

    return (
        <select>
            <option selected>{options[selectedOption].label}</option>
            {
                options.map((option, index) => {
                    if(index!==selectedOption){
                        return (
                            <option onClick={()=>{setSelectedOption(index)}}>{option.label}</option>
                        )
                    }
                })
            }
        </select> 
    )
}

export default DropDown